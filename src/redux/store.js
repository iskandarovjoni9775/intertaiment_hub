import { createStore,applyMiddleware } from "redux";
import thunk from "redux-thunk";
import {composeWithDevTools} from "redux-devtools-extension";
import { rootReduser } from "./reduser/rootReduser";

 
const middleware = [thunk];
const initialState = {};

export const store = createStore(rootReduser,initialState,composeWithDevTools(applyMiddleware(...middleware)));






