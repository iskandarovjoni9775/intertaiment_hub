import { LOADING_TRUE,GET_TRENDING,LOADING_FALSE
    , SINGLI_CINEMA, GET_SINGLI_CINEMA_VIDEO,
     GET_SINGLI_CINEMA_CREDITS, GET_MOVIES, 
     GET_MOVIES_GENRES_DATA, 
     SET_MOVIES_SELECTED_GENRES,
     GET_SERIES,
     SET_SERIES_SELECTED_GENRES,
     GET_SERIES_GENRES_DATA,
     SET_SEARCH_TEXT,
     GET_SEARCH_CINEMA_DATA} from "../actionTypes/actionType";

import axios from "axios";
import { PATH_NAME } from "../../tools/constant";
import { APIKey } from "../../APIKey";


export const getTrending = (page) => async (dispatch) => {

    dispatch({
        type: LOADING_TRUE
    });

    try {

        const res = await axios.get(`${PATH_NAME}/trending/all/week?api_key=${APIKey}&page=${page}`);
        dispatch({
            type: GET_TRENDING,
            payload:res.data,
        });
        
    

    } catch (err) {
        dispatch({
            type: LOADING_FALSE
        })
    }
};


export const getSingliCinema = (sinema_type,sinema_id) => async (dispatch) => {

    dispatch({
        type: LOADING_TRUE
    });

    try {

        const res = await axios.get(`https://api.themoviedb.org/3/${sinema_type}/${sinema_id}?api_key=${APIKey}&language=en-US`);
   

        dispatch({
             type: SINGLI_CINEMA,
             payload:res.data,
         });   

    } catch (err) {
        dispatch({
            type: LOADING_FALSE
        })
    }
};

export const getSingliCinemaVidoe = (sinema_type,sinema_id) => async (dispatch) => {

    dispatch({
        type: LOADING_TRUE
    });

    try {

        const res = await axios.get(`https://api.themoviedb.org/3/${sinema_type}/${sinema_id}/videos?api_key=${APIKey}&language=en-US`);
        console.log(res.data)
        dispatch({
             type:GET_SINGLI_CINEMA_VIDEO,
             payload:res.data,
         });   

    } catch (err) {
        dispatch({
            type: LOADING_FALSE
        })
    }
};


export const getSingliCinemaCredits = (sinema_type,sinema_id) => async (dispatch) => {

    dispatch({
        type: LOADING_TRUE
    });

    try {
        const res = await axios.get(`https://api.themoviedb.org/3/${sinema_type}/${sinema_id}/credits?api_key=${APIKey}&language=en-US`);
           dispatch({
             type:GET_SINGLI_CINEMA_CREDITS,
             payload:res.data,
         });   

    } catch (err) {
        dispatch({
            type: LOADING_FALSE
        })
    }
};

export const getMovies = (page,genreforURL) => async (dispatch) => {

    dispatch({
        type: LOADING_TRUE
    });

    try {
        const res = await axios.get(`https://api.themoviedb.org/3/discover/movie?api_key=${APIKey}&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=${page}&with_genres=${genreforURL}`);
           dispatch({
             type:GET_MOVIES,
             payload:res.data,
         });   

    } catch (err) {
        dispatch({
            type: LOADING_FALSE
        })
    }
};

export const getMoviesGenresData = (sinema_type) => async (dispatch) => {

    dispatch({
        type: LOADING_TRUE
    });

    try {

        const res = await axios.get(`https://api.themoviedb.org/3/genre/${sinema_type}/list?api_key=${APIKey}&language=en-us`);
  
        dispatch({
            type:GET_MOVIES_GENRES_DATA,
            payload: res.data
        });


    } 
    catch (err) {
        dispatch({
            type: LOADING_FALSE
        })
    }
};

export const setMoviesSelectedGenresData = (selected_genres) => async (dispatch) => {

    dispatch({
        type: LOADING_TRUE
    });

    try {
        dispatch({
            type: SET_MOVIES_SELECTED_GENRES,
            payload: selected_genres
        });


    } catch (err) {
        dispatch({
            type: LOADING_FALSE
        })
    }
};


export const getSeries = (page,genreforURL) => async (dispatch) => {

    dispatch({
        type: LOADING_TRUE
    });

    try {
        const res = await axios.get(`https://api.themoviedb.org/3/discover/tv?api_key=${APIKey}&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=${page}&with_genres=${genreforURL}`);
           dispatch({
             type:GET_SERIES,
             payload:res.data,
         });   

    } catch (err) {
        dispatch({
            type: LOADING_FALSE
        })
    }
};

export const getSeriesGenresData = (sinema_type) => async (dispatch) => {

    dispatch({
        type: LOADING_TRUE
    });

    try {

        const res = await axios.get(`https://api.themoviedb.org/3/genre/${sinema_type}/list?api_key=${APIKey}&language=en-us`);
  
        dispatch({
            type:GET_SERIES_GENRES_DATA,
            payload: res.data
        });


    } 
    catch (err) {
        dispatch({
            type: LOADING_FALSE
        })
    }
};

export const setSeriesSelectedGenresData = (selected_genres) => async (dispatch) => {

    dispatch({
        type: LOADING_TRUE
    });

    try {
        dispatch({
            type: SET_SERIES_SELECTED_GENRES,
            payload: selected_genres
        });


    } catch (err) {
        dispatch({
            type: LOADING_FALSE
        })
    }
};


// export const getSearchCinemaData = (type,searchText,page) => async (dispatch) => {

//     dispatch({
//         type: LOADING_TRUE
//     });

//     try {

//         const res = axios.get(`https://api.themoviedb.org/3/search/${type?"tv":"movie"}?api_key=${APIKey}&language=en-US&query=${searchText}&page=${page}&include_adult=false`)
        
//         dispatch({
//             type:GET_SEARCH_CINEAM_DATA,
//             payload:res.data
//         })
        
//         dispatch({
//             type:SET_SEARCH_TEXT,
//             payload:{
//                 searchText,
//                 currentPage:page,
//                 currentType:type,
//             }
//         });


//     } catch (err) {
//         dispatch({
//             type: LOADING_FALSE
//         })
//     }
// };
export const getSearchCinemaData = (type, searchText, page) => async (dispatch) => {

    dispatch({
        type: LOADING_TRUE
    });

    try {

        const res = await axios.get(`${PATH_NAME}/search/${type ? "tv" : "movie"}?api_key=${APIKey}&language=en-US&query=${searchText}&page=${page}&include_adult=false`);
        dispatch({
            type: GET_SEARCH_CINEMA_DATA,
            payload: res.data
        });


        dispatch({
            type: SET_SEARCH_TEXT,
            payload: {
                searchText,
                currentPage: page,
                currentType: type
            }
        });


    } catch (err) {

        dispatch({
            type: LOADING_FALSE
        })
    }
}

