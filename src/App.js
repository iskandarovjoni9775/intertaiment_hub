import'./App.css'
import React, { useEffect } from 'react';
import Header from "./components/Header/Header"
import Footer from './components/Footer/Footer';
import { Switch } from 'react-router-dom';
import { Route } from 'react-router-dom';
import Trending from './pages/Trending/Trending';
import Movies from './pages/Movies/Movies';
import Search from './pages/Search/Search';
import Info from './pages/Info/Info';
import { getMoviesGenresData, getSeriesGenresData } from './redux/action/interteimentaction';
import { useDispatch } from 'react-redux';
import Series from './pages/Series/Series';

function App(props) {


  const dispatch = useDispatch();
  useEffect(()=>{
    dispatch(getMoviesGenresData("movie"))
    dispatch(getSeriesGenresData("tv"))
      
    
   
  })

  
  return (
    <>
    <Header/>
    <div className='App'>
      <Switch>
        <Route exact path="/" component={Trending}/>
        <Route exact path="/movies" component={Movies}/>
        <Route exact path="/series" component={Series}/>
        <Route exact path="/search" component={Search}/>
        <Route exact path="/info/:type/:id" component={Info}/>
      </Switch>
    </div>        
    <Footer/>
    </>
  );
}

export default App;
