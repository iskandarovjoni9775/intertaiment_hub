import React, { useEffect } from "react";
import BottomNavigation from '@mui/material/BottomNavigation';
import BottomNavigationAction from '@mui/material/BottomNavigationAction';
import RestoreIcon from '@mui/icons-material/Restore';
import FavoriteIcon from '@mui/icons-material/Favorite';
import LocationOnIcon from '@mui/icons-material/LocationOn';
import WhatshotIcon from '@mui/icons-material/Whatshot';
import SearchIcon from '@mui/icons-material/Search';
import LiveTvIcon from '@mui/icons-material/LiveTv';
import SmartDisplayIcon from '@mui/icons-material/SmartDisplay';

import { useHistory } from "react-router-dom";
import "./footer.css";

export default function Footer() {
  const [value, setValue] = React.useState(0);

  const history=useHistory();
  useEffect(()=>{
    if(value === 0) history.push('/')
    else if(value === 1) history.push("/movies");
    else if(value === 2) history.push("/series");
    else if(value === 3) history.push("/search");
  })

  return (
    
      <BottomNavigation
        showLabels
        value={value}
        onChange={(event, newValue) => {
          setValue(newValue);
          
        }}
        style={{
            width: "100%",
            position: "fixed",
            bottom: "0",
            backgroundColor: "#39445a",
            ZIndex: 100,
            height: "70px",
            boxShadow: "1px 0px 5px black"
        }}
      >
        <BottomNavigationAction
         label="Trending"
         icon={<WhatshotIcon />}
         style={{color: "white"}} />
        
        <BottomNavigationAction 
        label="Movies" 
        icon={<LiveTvIcon />}
        style={{color: "white"}} />
          <BottomNavigationAction 
        label="Series" 
        icon={<SmartDisplayIcon /> } 
        style={{color: "white"}}/>
        
        <BottomNavigationAction 
        label="Search"
         icon={<SearchIcon />} 
         style={{color: "white"}}/>
      
      
      
      </BottomNavigation>
  
  );
}




