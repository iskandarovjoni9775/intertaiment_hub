import React, { useEffect } from 'react'
import {useDispatch, useSelector} from "react-redux"
import { getSingliCinema, getSingliCinemaVidoe,getSingliCinemaCredits } from '../../redux/action/interteimentaction';
import { useParams } from 'react-router-dom';
import { img_500, unavailable, unavailableLandscape } from '../../config/config';
import "./Info.css";
import Button from "@mui/material/Button";
import YouTubeIcon from '@mui/icons-material/YouTube';
import Carousel from '../../components/Carousel/Carousel';

export default function Info() {
   const {type,id} = useParams();
  const dispatch = useDispatch();

  const {singlicinema,singlicinemaVideo,singlecinemaCredits} = useSelector(state=>state.intertaiment);
  useEffect(()=>{
    console.log(type,id)
    dispatch(getSingliCinema(type,id))
    dispatch(getSingliCinemaVidoe(type,id))
    dispatch(getSingliCinemaCredits(type,id))
  },[type,id])
  return (
    <div className="container">

    <div className="info-page">
        {
            singlicinema && (

                <div className="Content-info">


                    <div className="row">


                        <div className="col-md-6">
                            <div className="img-box">
                                <img  className="Content-info-portrait"
                                      src={singlicinema.poster_path ? `${img_500}/${singlicinema.poster_path}` : unavailable}
                                      alt={singlicinema.name || singlicinema.title}
                                />
                                <img  className="Content-info-landscape"
                                      src={singlicinema.backdrop_path ? `${img_500}/${singlicinema.backdrop_path}` : unavailableLandscape}
                                      alt={singlicinema.name || singlicinema.title}
                                />
                            </div>
                        </div>


                        <div className="col-md-6">

                            <div className="Content-info-about">
                                <div>
                                    <div className="Content-info-title">
                                        {singlicinema.name || singlicinema.title}(
                                        {
                                            (
                                              singlicinema.first_air_date || singlicinema.release_date || "......"
                                            ).substring(0,4)
                                        }
                                        )
                                    </div>

                                    {
                                        singlicinema.tagline && (
                                            <div className="tagline">
                                                <i>{singlicinema.tagline}</i>
                                            </div>
                                        )
                                    }
                                </div>


                                <div className="Content-info-description">
                                    {singlicinema.overview ? singlicinema.overview : 'No description'}
                                </div>


                                {
                                    singlecinemaCredits.length > 1 ?
                                        <div>
                                            <Carousel/>
                                        </div> : ""
                                }


                                <Button
                                    variant="contained"
                                    startIcon={<YouTubeIcon/>}
                                    color="secondary"
                                    target="_blank"
                                    href={`https://www.youtube.com/watch?v=${singlicinemaVideo}`}
                                    className="my-button"
                                >
                                    Watch The Trailer
                                </Button>

                            </div>
                        </div>
                    </div>
                </div>
            )
        }
    </div>
</div>
  )
}
