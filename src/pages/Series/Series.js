import React, { useEffect, useState } from 'react';
import { useSelector,useDispatch } from 'react-redux';
import CustomPagination from "../../components/Pagination/CustomPagination";
import SingleContent from "../../components/SingleContent/SingleContent";
import Genres from '../../components/Genres/Genres';
import { getSeries } from '../../redux/action/interteimentaction';


function Series(props){

    const [page, setPage] = useState(1);
    const dispatch=useDispatch();
    const {series,series_selected_genres,seriesNumberOfPages}=useSelector(state=>state.intertaiment);

    useEffect(()=>{
        dispatch(getSeries(page,series_selected_genres))
    },[page,series_selected_genres])

    return (
        
        <div className="container">

        <div className="pageTitle">Tv Series</div>

        <Genres
            type="tv"
            setPage={setPage}
        />

        
                <div>

                    <div className="series">

                        <div className="row">
                            {
                                series.length >= 1 ?

                                    series.map((item, index) => (
                                        <div className="col-md-3" key={item.id}>
                                            <SingleContent
                                                id={item.id}
                                                poster={item.poster_path}
                                                title={item.title || item.name}
                                                date={item.first_air_date || item.release_date}
                                                media_type="movie"
                                                vote_average={item.vote_average}
                                            />
                                        </div>))

                                    : <h1 className="text-center mt-5">Serie not found !!!</h1>
                            }

                        </div>
                    </div>



                    {
                        seriesNumberOfPages > 1 ?

                            <CustomPagination page={page} setPage={setPage} numberOfPages={seriesNumberOfPages}/>

                            : ""
                    }


                </div>

        

    </div>
    );
};
export default Series;
